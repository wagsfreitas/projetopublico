package br.com.wagsf.digito_unico;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.wagsf.Controller.SingleDigitController;
import br.com.wagsf.Model.SingleDigit;
import br.com.wagsf.Model.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javassist.bytecode.analysis.MultiType;

@Path("usuario")
@Api(value = "Usuario")
public class UserResource
{
	@GET
	@Produces(MediaType.APPLICATION_XML)
	@Path("{name}")
	@ApiOperation(value = "Obter dados do usuario")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Ok", response = User.class),
			@ApiResponse(code = 202, message = "Usuario nao cadastrado!") })
	public Response getUser(@PathParam("name") String p_UserName)
	{
		User v_User = SingleDigitController.getUser(p_UserName);

		return v_User != null ? Response.status(200).entity(v_User).build()
				: Response.status(202).entity("Usuario nao cadastrado").build();
	}

	@POST
	@Produces(MediaType.APPLICATION_XML)
	@Path("{name}/{email}")
	@ApiOperation(value = "Cadastrar usuario")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Usuario ja cadastrado"),
			@ApiResponse(code = 202, message = "Usuario nao cadastrado!"),
			@ApiResponse(code = 201, message = "Usuario cadastrado com sucesso") })
	public Response insertUser(@PathParam("name") String p_UserName, @PathParam("email") String p_Email)
	{
		if (SingleDigitController.getUser(p_UserName) != null)
		{
			return Response.status(200).entity("Usuario ja cadastrado").build();
		}
		if (SingleDigitController.insertUser(p_UserName, p_Email) == null)
		{
			return Response.status(202).entity("Usuario nao cadastrado").build();
		}
		else
		{
			return Response.status(201).entity("Usuario cadastrado com sucesso").build();
		}
	}

	@PUT
	@Produces(MediaType.TEXT_PLAIN)
	@Path("{name}/{email}")
	@ApiOperation(value = "Atualizar e-mail do usuario")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Usuario ja cadastrado"),
			@ApiResponse(code = 202, message = "Usuario nao cadastrado"),
			@ApiResponse(code = 423, message = "Nao foi possivel atualizar os dados") })
	public Response updateUser(@PathParam("name") String p_UserName, @PathParam("email") String p_Email)
	{
		if (SingleDigitController.getUser(p_UserName) == null)
		{
			return Response.status(202).entity("Usuario nao cadastrado").build();
		}
		if (SingleDigitController.updateUser(p_UserName, p_Email))
		{
			return Response.status(201).entity("Dados atualizados com sucesso").build();
		}
		else
		{
			return Response.status(423).entity("Nao foi possivel atualizar os dados").build();
		}
	}

	@DELETE
	@Produces(MediaType.TEXT_PLAIN)
	@Path("{name}")
	@ApiOperation(value = "Remover usuario")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Usuario removido"),
			@ApiResponse(code = 202, message = "Usuario nao cadastrado") })
	public Response deleteUser(@PathParam("name") String p_UserName)
	{
		return SingleDigitController.deleteUser(p_UserName) != 0 ? Response.status(200).entity("Usuario deletado").build() :
			Response.status(202).entity("Usuario nao cadastrado").build();
	}

	@PUT
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.TEXT_HTML)
	@Path("chave/{user}")
	@ApiOperation(value = "Cadastrar chave publica do usuario")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Chave cadastrada"),
			@ApiResponse(code = 202, message = "Usuario nao cadastrado") })
	public Response postUserKey(@PathParam("user") String p_UserName, String p_UserKey)
	{
		return SingleDigitController.postUserKey(p_UserName, p_UserKey) ? Response.status(200).entity("Chave cadastrada").build()
				: Response.status(202).entity("Usuario nao cadastrado").build();
	}

}
