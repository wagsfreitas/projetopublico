package br.com.wagsf.digito_unico;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.sun.xml.txw2.annotation.XmlElement;

import br.com.wagsf.Controller.SingleDigitController;
import br.com.wagsf.Model.SingleDigit;
import br.com.wagsf.Model.SingleDigitList;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Path("digito-unico/")
@Api(value = "Digito Unico")
public class SingleDigitResource
{
	@GET
	@Produces(MediaType.APPLICATION_XML)
	@Path("{number}/{repetitionFactor}")
	@ApiOperation(value = "Calcular digito unico")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Ok", response = SingleDigit.class),
			@ApiResponse(code = 422, message = "Parâmetros não suportados!") })
	public Response calculateSingleDigit(@PathParam("number") String p_Number,
			@PathParam("repetitionFactor") int p_RepetitionFactor)
	{
		SingleDigit v_Return = SingleDigitController.getSingleDigit(p_Number, p_RepetitionFactor);

		return v_Return != null ? Response.status(200).entity(v_Return).build()
				: Response.status(422).entity("Parametros nao suportados").build();
	}

	@GET
	@Produces(MediaType.APPLICATION_XML)
	@Path("{number}/{repetitionFactor}/{user}")
	@ApiOperation(value = "Calcular digito unico atrelado a usuario")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Ok", response = SingleDigitList.class),
			@ApiResponse(code = 422, message = "Parâmetros não suportados!") })
	public Response calculateSDBoundToUser(@PathParam("number") String p_Number,
			@PathParam("repetitionFactor") int p_RepetitionFactor, @PathParam("user") String p_UserName)
	{
		SingleDigit v_Return = SingleDigitController.getSingleDigit(p_Number, p_RepetitionFactor, p_UserName);

		return v_Return != null ? Response.status(200).entity(v_Return).build()
				: Response.status(422).entity("Parametros nao suportados").build();
	}

	@GET
	@Produces(MediaType.APPLICATION_XML)
	@Path("{user}")
	@ApiModelProperty(hidden = true)
	@ApiOperation(value = "Listar cálculos de digito unico feitos para o usuario")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Ok", response = SingleDigit.class),
			@ApiResponse(code = 202, message = "Nenhum resultado encontrado!") })
	public Response listByUser(@PathParam("user") String p_UserName)
	{
		SingleDigitList v_ReturnList = new SingleDigitList(SingleDigitController.getUser(p_UserName).getSingleDigitList());
		
		return v_ReturnList.getSize() != 0 ? Response.status(204).entity(v_ReturnList).build()
				: Response.status(202).entity("Nenhum resultado encontrado").build();
	}
}
