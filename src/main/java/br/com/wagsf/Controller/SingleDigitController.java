package br.com.wagsf.Controller;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

import br.com.wagsf.Model.SingleDigit;
import br.com.wagsf.Model.User;

@XmlRootElement
public class SingleDigitController
{
	public static SingleDigit getSingleDigit(String p_Number, int p_RepetitionFactor)
	{
		return getSingleDigit(p_Number, p_RepetitionFactor, null);
	}

	public static ArrayList<User> getUsersList()
	{
		return User.getUserList();
	}

	public static User insertUser(String p_Name, String p_Email)
	{
		return User.provide(p_Name, p_Email);
	}

	public static User getUser(String p_Name)
	{
		return User.getUser(p_Name);
	}

	public static int deleteUser(String p_Name)
	{
		User v_User = User.getUser(p_Name);
		return v_User == null ? 0 : v_User.deleteUser();
		
	}

	public static SingleDigit getSingleDigit(String p_Number, int p_RepetitionFactor, String p_UserName)
	{
		User v_User = null;
		
		if(p_UserName != null)
		{
			v_User = User.getUser(p_UserName);
		}
		
		SingleDigit v_SingleDigit = SingleDigit.provideSingleDigit(p_Number, p_RepetitionFactor, v_User);
		
		return  v_SingleDigit;
	}

	public static ArrayList<SingleDigit> getSDListByUser(String p_UserName)
	{
		User v_User = User.getUser(p_UserName);
		
		if (v_User == null)
		{
			return null;
		}
		
		return v_User.getSingleDigitList();
	}

	public static boolean updateUser(String p_UserName, String p_Email)
	{
		User v_User = User.getUser(p_UserName);
		return v_User.updateEmail(p_Email);
	}

	public static boolean postUserKey(String p_UserName, String p_UserKey)
	{
		User v_User = User.getUser(p_UserName);
		
		if(v_User == null)
		{
			return false;
		}
		
		v_User.updateUserKey(p_UserKey);
		return true;
	}

}
