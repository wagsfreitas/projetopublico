package br.com.wagsf.Model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class UserTM
{
	public static Connection m_DBConnection;
	public static boolean m_HasData = false;

	private static void ensureConnection() throws ClassNotFoundException, SQLException
	{
		if (m_DBConnection == null)
		{
			getConnection();
		}
	}

	public static Connection getDBConnection()
	{
		return m_DBConnection;
	}
	
	private static void getConnection() throws ClassNotFoundException, SQLException
	{
		Class.forName("org.sqlite.JDBC");
		m_DBConnection = DriverManager
				.getConnection("jdbc:sqlite:C:/Users/wagsf/eclipse-workspace/digito-unico/SQLiteSingleDigit.db");
		initialize();
	}
	
	private static void initialize()
	{
		if (!m_HasData)
		{
			try
			{
				Statement v_Statement = m_DBConnection.createStatement();
				ResultSet v_ResultSet = v_Statement
						.executeQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='user'");
				if (!v_ResultSet.next())
				{
					Statement v_CreteTableStatement = m_DBConnection.createStatement();
					v_CreteTableStatement.executeUpdate("CREATE TABLE user(id integer ," + "name varchar(60) unique,"
							+ "email varchar(60)," + "key varchar(500)," + "primary key(id));");
				}
				v_Statement = m_DBConnection.createStatement();
				v_ResultSet = v_Statement
						.executeQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='singleDigit'");
				if (!v_ResultSet.next())
				{
					Statement v_CreteTableStatement = m_DBConnection.createStatement();
					v_CreteTableStatement.executeUpdate("CREATE TABLE singleDigit(id integer, " + "userId integer, "
							+ "inputInteger varchar(60), " + "numberOfRepetitions integer, " + "result integer, "
							+ "primary key(id)," + "foreign key(userId) references user(id));");
				}
			}
			catch (SQLException e)
			{
				e.printStackTrace();
			}
			m_HasData = true;
		}
	}
	
	private static int getLastInsertedId(String p_Table) throws SQLException
	{
		Statement v_Statement = UserTM.getDBConnection().createStatement();
		ResultSet v_ResultSet = v_Statement.executeQuery("SELECT MAX(id) AS id  FROM " + p_Table);
		return v_ResultSet.getInt("id");
	}
	
	public static ArrayList<User> getUserList() throws ClassNotFoundException, SQLException
	{
		ensureConnection();
		Statement v_Statement = m_DBConnection.createStatement();
		ResultSet v_ResultSet = v_Statement.executeQuery("SELECT id, name, email, key FROM user");
		ArrayList<User> v_UserList = new ArrayList<User>();

		while (v_ResultSet.next())
		{
			v_UserList.add(
					new User(v_ResultSet.getInt("id"), v_ResultSet.getString("name"), v_ResultSet.getString("email"),
							v_ResultSet.getString("key")));
		}

		return v_UserList;
	}


	public static int insertUser(User p_User) throws ClassNotFoundException, SQLException
	{
		ensureConnection();
		PreparedStatement v_PreparedStatement = m_DBConnection.prepareStatement("INSERT INTO user VALUES(null,?,?,null);");
		v_PreparedStatement.setString(1, p_User.getName());
		v_PreparedStatement.setString(2, p_User.getEmailAdress());
		v_PreparedStatement.execute();

		return getLastInsertedId("user");
	}

	public static User getUserByName(String p_Name) throws ClassNotFoundException, SQLException
	{
		ensureConnection();
		PreparedStatement v_PreparedStatement = m_DBConnection
				.prepareStatement("SELECT id, name, email, key FROM user WHERE name = ?;");
		v_PreparedStatement.setString(1, p_Name);
		ResultSet v_Results = v_PreparedStatement.executeQuery();

		return v_Results.next()
				? new User(v_Results.getInt("id"), v_Results.getString("name"), v_Results.getString("email"),
						v_Results.getString("key"))
				: null;
	}

	public static boolean updateUserEmail(User p_User) throws ClassNotFoundException, SQLException
	{
		ensureConnection();
		PreparedStatement v_PreparedStatement = m_DBConnection
				.prepareStatement("UPDATE user SET email = ? WHERE id = ?;");
		v_PreparedStatement.setString(1, p_User.getEmailAdress());
		v_PreparedStatement.setInt(2, p_User.getPK());

		return v_PreparedStatement.execute();
	}

	public static boolean updateUserKey(User p_User, String p_Key) throws ClassNotFoundException, SQLException
	{
		ensureConnection();
		PreparedStatement v_PreparedStatement = m_DBConnection
				.prepareStatement("UPDATE user SET key = ? WHERE id = ?;");
		v_PreparedStatement.setString(1, p_Key);
		v_PreparedStatement.setInt(2, p_User.getPK());

		return v_PreparedStatement.execute();
	}

	public static int deleteUser(int p_UserPk) throws SQLException, ClassNotFoundException
	{
		ensureConnection();
		PreparedStatement v_PreparedStatement = m_DBConnection.prepareStatement("DELETE FROM user WHERE id = ?;");
		v_PreparedStatement.setInt(1, p_UserPk);

		return v_PreparedStatement.executeUpdate();
	}

	public static int deleteSingleDigit(Integer p_PK) throws SQLException, ClassNotFoundException
	{
		if (p_PK != null)
		{
			ensureConnection();
			PreparedStatement v_PreparedStatement = m_DBConnection
					.prepareStatement("DELETE FROM singleDigit WHERE id = ?;");
			v_PreparedStatement.setInt(1, p_PK.intValue());
			return v_PreparedStatement.executeUpdate();
		}
		return 0;
	}

	public static int insertSingleDigit(SingleDigit p_SingleDigit) throws ClassNotFoundException, SQLException
	{
		ensureConnection();

		PreparedStatement v_PreparedStatement = m_DBConnection
				.prepareStatement("INSERT INTO singleDigit VALUES(null,?,?,?,?);");
		v_PreparedStatement.setInt(1, p_SingleDigit.getUserPK());
		v_PreparedStatement.setString(2, p_SingleDigit.getInputInteger());
		v_PreparedStatement.setInt(3, p_SingleDigit.getNumberOfRepetitions());
		v_PreparedStatement.setInt(4, p_SingleDigit.getResult());
		v_PreparedStatement.execute();

		return getLastInsertedId("singleDigit");
	}

	public static ArrayList<SingleDigit> listSDByUser(int p_UserPK) throws ClassNotFoundException, SQLException
	{
		ensureConnection();
		PreparedStatement v_PreparedStatement = m_DBConnection.prepareStatement(
				"SELECT id, userId, inputInteger, numberOfRepetitions, result FROM singleDigit WHERE userId = ?;");
		v_PreparedStatement.setInt(1, p_UserPK);
		ResultSet v_ResultSet = v_PreparedStatement.executeQuery();
		ArrayList<SingleDigit> v_SingleDigitList = new ArrayList<SingleDigit>();

		while (v_ResultSet.next())
		{
			v_SingleDigitList.add(
					new SingleDigit(v_ResultSet.getInt("id"), v_ResultSet.getInt("userId"),
							v_ResultSet.getString("inputInteger"), v_ResultSet.getInt("numberOfRepetitions"),
							v_ResultSet.getInt("result")));
		}

		return v_SingleDigitList;
	}
}
