package br.com.wagsf.Model;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class SingleDigitList
{
	ArrayList<SingleDigit> m_SingleDigitList;

	public SingleDigitList()
	{
		this.m_SingleDigitList = new ArrayList<SingleDigit>();
	}

	public SingleDigitList(ArrayList<SingleDigit> p_SingleDigitList)
	{
		this.m_SingleDigitList = p_SingleDigitList;
	}

	public ArrayList<SingleDigit> getList()
	{
		return m_SingleDigitList;
	}

	public void setList(ArrayList<SingleDigit> p_SingleDigitList)
	{
		this.m_SingleDigitList = p_SingleDigitList;
	}

	public void addList(ArrayList<SingleDigit> p_SingleDigitList)
	{
		this.m_SingleDigitList.addAll(p_SingleDigitList);
	}

	public int getSize()
	{
		return m_SingleDigitList == null ? 0 : m_SingleDigitList.size();
	}

	public void add(SingleDigit p_SingleDigit)
	{
		m_SingleDigitList.add(p_SingleDigit);
	}
}
