package br.com.wagsf.Model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class SingleDigitTM
{
	public static Connection m_DBConnection;
	public static boolean m_HasData = false;

	public static ArrayList<SingleDigit> listSDByUser(int p_UserPK) throws ClassNotFoundException, SQLException
	{
		ensureConnection();
		PreparedStatement v_PreparedStatement = m_DBConnection.prepareStatement(
				"SELECT id, userId, inputInteger, numberOfRepetitions, result FROM singleDigit WHERE userId = ?;");
		v_PreparedStatement.setInt(1, p_UserPK);
		ResultSet v_ResultSet = v_PreparedStatement.executeQuery();
		ArrayList<SingleDigit> v_SingleDigitList = new ArrayList<SingleDigit>();

		while (v_ResultSet.next())
		{
			v_SingleDigitList.add(
					new SingleDigit(v_ResultSet.getInt("id"), v_ResultSet.getInt("userId"),
							v_ResultSet.getString("inputInteger"), v_ResultSet.getInt("numberOfRepetitions"),
							v_ResultSet.getInt("result")));
		}

		return v_SingleDigitList;
	}

	public static int insertSingleDigit(SingleDigit p_SingleDigit) throws ClassNotFoundException, SQLException
	{
		ensureConnection();

		PreparedStatement v_PreparedStatement = m_DBConnection
				.prepareStatement("INSERT INTO singleDigit VALUES(null,?,?,?,?);");
		v_PreparedStatement.setInt(1, p_SingleDigit.getUserPK());
		v_PreparedStatement.setString(2, p_SingleDigit.getInputInteger());
		v_PreparedStatement.setInt(3, p_SingleDigit.getNumberOfRepetitions());
		v_PreparedStatement.setInt(4, p_SingleDigit.getResult());
		v_PreparedStatement.execute();

		return getLastInsertedId();
	}

	private static int getLastInsertedId() throws SQLException
	{
		Statement v_Statement = UserTM.getDBConnection().createStatement();
		ResultSet v_ResultSet = v_Statement.executeQuery("SELECT MAX(id) AS id  FROM singleDigit");
		return v_ResultSet.getInt("id");
	}

	private static void getConnection() throws ClassNotFoundException, SQLException
	{
		Class.forName("org.sqlite.JDBC");
		m_DBConnection = DriverManager
				.getConnection("jdbc:sqlite:C:/Users/wagsf/eclipse-workspace/digito-unico/SQLiteSingleDigit.db");
	}

	private static void initialize()
	{
		if (!m_HasData)
		{
			try
			{
				Statement v_Statement = m_DBConnection.createStatement();
				ResultSet v_ResultSet = v_Statement
						.executeQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='singleDigit'");
				if (!v_ResultSet.next())
				{
					System.out.println("Construindo Tabela");
					Statement v_CreteTableStatement = m_DBConnection.createStatement();
					v_CreteTableStatement.executeUpdate("CREATE TABLE singleDigit(id integer, " + "userId integer, "
							+ "inputInteger varchar(60), " + "numberOfRepetitions integer, " + "result integer, "
							+ "primary key(id)," + "foreign key(userId) references user(id));");
				}
			}
			catch (SQLException e)
			{
				e.printStackTrace();
			}
			m_HasData = true;
		}
	}

	public static SingleDigit getSingleDigit(int p_PK) throws ClassNotFoundException, SQLException
	{
		ensureConnection();
		PreparedStatement v_PreparedStatement = m_DBConnection.prepareStatement(
				"SELECT id, userId, inputInteger, numberOfRepetitions, result FROM singleDigit WHERE id = ?;");
		v_PreparedStatement.setInt(1, p_PK);
		ResultSet v_Results = v_PreparedStatement.executeQuery();

		return v_Results.next()
				? new SingleDigit(v_Results.getInt("id"), v_Results.getInt("userId"),
						v_Results.getString("inputInteger"), v_Results.getInt("numberOfRepetitions"),
						v_Results.getInt("result"))
				: null;
	}

	public static int deleteSingleDigit(Integer p_PK) throws SQLException, ClassNotFoundException
	{
		if (p_PK != null)
		{
			ensureConnection();
			PreparedStatement v_PreparedStatement = m_DBConnection
					.prepareStatement("DELETE FROM singleDigit WHERE id = ?;");
			v_PreparedStatement.setInt(1, p_PK.intValue());
			return v_PreparedStatement.executeUpdate();
		}
		return 0;
	}

	private static void ensureConnection() throws ClassNotFoundException, SQLException
	{
		if (m_DBConnection == null)
		{
			m_DBConnection = UserTM.getDBConnection();
			
			if (m_DBConnection == null)
			{
				getConnection();
			}
			
			initialize();
		}
	}

}
