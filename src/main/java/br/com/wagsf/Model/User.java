package br.com.wagsf.Model;

import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.xml.bind.annotation.XmlRootElement;

import io.swagger.annotations.ApiModelProperty;


@XmlRootElement
public class User
{
	private String m_Name;

	private String m_EmailAddress;

	private ArrayList<SingleDigit> m_SingleDigitList;

	@ApiModelProperty(hidden = true)
	private int m_PK;

	private String m_Key;

	public User(int p_PK, String p_Name, String p_EmailAddressAdress, String p_UserKey) throws ClassNotFoundException, SQLException
	{
		this(p_Name, p_EmailAddressAdress);
		setPK(p_PK);
		setKey(p_UserKey);
		m_SingleDigitList.addAll(SingleDigit.listSDByUser(m_PK));
	}

	public User()
	{
		setName("");
		setEmailAdress("");
		setPK(0);
		setKey(null);
		m_SingleDigitList = new ArrayList<SingleDigit>();
	}

	public User(String p_Name, String p_EmailAddress)
	{
		setName(p_Name);
		setEmailAdress(p_EmailAddress);
		setKey(null);
		m_SingleDigitList = new ArrayList<SingleDigit>();
	}

	public String getName()
	{
		return getKey() == null ? m_Name : encrypt(m_Name);
	}

	public void setName(String p_Name)
	{
		m_Name = p_Name;
	}

	public String getEmailAdress()
	{
		return m_Key == null ? m_EmailAddress : encrypt(m_EmailAddress);
	}

	public void setEmailAdress(String p_EmailAddressAdress)
	{
		m_EmailAddress = p_EmailAddressAdress;
	}

	public ArrayList<SingleDigit> getSingleDigitList()
	{
		return m_SingleDigitList;
	}

	public void setSingleDigitList(ArrayList<SingleDigit> p_SingleDigitList)
	{
		m_SingleDigitList = p_SingleDigitList;
	}

	@ApiModelProperty(hidden = true)
	public int getPK()
	{
		return m_PK;
	}

	private void setPK(int p_PK)
	{
		m_PK = p_PK;

	}

	public void addResult(SingleDigit p_SingleDigit)
	{
		if (getSingleDigit(p_SingleDigit.getInputInteger(), p_SingleDigit.getNumberOfRepetitions()) == null)
		{
			m_SingleDigitList.add(p_SingleDigit);
		}
	}

	public void addResultList(ArrayList<SingleDigit> p_SingleDigit)
	{
		m_SingleDigitList.addAll(p_SingleDigit);
	}

	public static ArrayList<User> getUserList()
	{
		ArrayList<User> v_UserList = null;
		try
		{
			v_UserList = UserTM.getUserList();
		}
		catch (ClassNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}

		return v_UserList;
	}

	public static User provide(String p_Name, String p_EmailAddress)
	{
		User v_User = null;
		try
		{
			v_User = UserTM.getUserByName(p_Name);
			if (v_User != null)
			{
				return v_User;
			}

			v_User = new User(p_Name, p_EmailAddress);
			v_User.setPK(UserTM.insertUser(v_User));
			v_User.addResultList(SingleDigit.listSDByUser(v_User.getPK()));
		}
		catch (ClassNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}

		return v_User;
	}

	public static User getUser(String p_Name)
	{
		User v_User = null;
		try
		{
			v_User = UserTM.getUserByName(p_Name);
		}
		catch (ClassNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}

		return v_User;
	}

	public int deleteUser()
	{
		try
		{
			SingleDigit.deleteAll(this.getSingleDigitList());
			return UserTM.deleteUser(this.getPK());
		}
		catch (ClassNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}

		return 0;
	}

	public SingleDigit getSingleDigit(String p_Number, int p_RepetitionFactor)
	{
		for (SingleDigit c_SingleDigit : m_SingleDigitList)
		{
			if(c_SingleDigit.getInputInteger().equalsIgnoreCase(p_Number) && c_SingleDigit.getNumberOfRepetitions() == p_RepetitionFactor)
			{
				return c_SingleDigit;
			}
		}
		
		return null;
	}

	public boolean updateEmail(String p_Email)
	{
		try
		{
			setEmailAdress(p_Email);
			UserTM.updateUserEmail(this);
			return true;
		}
		catch (ClassNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		
		return false;
	}

	private String getKey()
	{
		return m_Key;
	}

	public void setKey(String p_UserKey)
	{
		m_Key = p_UserKey;
	}
	
	private String encrypt(String p_InformationString)
	{
		try
		{
			byte[] v_KeyBytes = Base64.getDecoder().decode(getKey());
			X509EncodedKeySpec v_KeySpec = new X509EncodedKeySpec(v_KeyBytes);
			KeyFactory v_Factory = KeyFactory.getInstance("RSA");
			PublicKey v_PublicKey = v_Factory.generatePublic(v_KeySpec);
			
			Cipher v_Cipher = Cipher.getInstance("RSA");
			v_Cipher.init(Cipher.ENCRYPT_MODE, v_PublicKey);
			return Base64.getEncoder().encodeToString(v_Cipher.doFinal(p_InformationString.getBytes()));
		}
		catch (NoSuchAlgorithmException e)
		{
			e.printStackTrace();
		}
		catch (InvalidKeySpecException e)
		{
			e.printStackTrace();
		}
		catch (NoSuchPaddingException e)
		{
			e.printStackTrace();
		}
		catch (InvalidKeyException e)
		{
			e.printStackTrace();
		}
		catch (IllegalBlockSizeException e)
		{
			e.printStackTrace();
		}
		catch (BadPaddingException e)
		{
			e.printStackTrace();
		}
		
		return null;
	}

	public void updateUserKey(String p_UserKey)
	{
		setKey(p_UserKey);
		try
		{
			UserTM.updateUserKey(this, p_UserKey);
		}
		catch (ClassNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
}
