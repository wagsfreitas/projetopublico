package br.com.wagsf.Model;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.sun.xml.txw2.annotation.XmlElement;

import io.swagger.annotations.ApiModelProperty;

@XmlRootElement
public class SingleDigit
{
	private String m_InputInteger;

	private int m_NumberOfRepetitions;

	private int m_Result;

	@ApiModelProperty(hidden = true)
	private Integer m_UserPK;

	private int m_PK;

	public static SingleDigit[] m_Cache = new SingleDigit[10];

	public static int m_CacheIterator = 0;

	public SingleDigit(Integer p_PK, int p_UserPK, String p_InputInteger, int p_NumberOfRepetitions, int p_Result)
	{
		this(p_UserPK, p_InputInteger, p_NumberOfRepetitions, p_Result);
		setPK(p_PK);
	}

	public SingleDigit()
	{
		setPK(0);
		setInputInteger(null);
		setNumberOfRepetitions(0);
		setResult(0);
	}

	public SingleDigit(Integer p_UserPK, String p_InputInteger, int p_NumberOfRepetitions, int p_Result)
	{
		setUserPK(p_UserPK);
		setInputInteger(p_InputInteger);
		setNumberOfRepetitions(p_NumberOfRepetitions);
		setResult(p_Result);
	}

	public String getInputInteger()
	{
		return m_InputInteger;
	}

	public void setInputInteger(String p_InputInteger)
	{
		m_InputInteger = p_InputInteger;
	}

	public int getNumberOfRepetitions()
	{
		return m_NumberOfRepetitions;
	}

	public void setNumberOfRepetitions(int p_NumberOfRepetitions)
	{
		m_NumberOfRepetitions = p_NumberOfRepetitions;
	}

	public int getResult()
	{
		return m_Result;
	}

	public void setResult(int p_Result)
	{
		m_Result = p_Result;
	}

	public int getUserPK()
	{
		return m_UserPK;
	}

	@ApiModelProperty(hidden = true)
	public void setUserPK(Integer p_UserName)
	{
		m_UserPK = p_UserName;
	}

	private void setPK(int p_PK)
	{
		m_PK = p_PK;
	}

	private Integer getSingleDigitPK()
	{
		return m_PK;
	}

	public static int calculateSingleDigit(String p_Number, int p_RepetitionFactor)
	{
		StringBuilder v_ConcatenatedNumber = new StringBuilder();
		int v_Counter = 0;

		for (int i = 0; i < p_RepetitionFactor; i++)
		{
			v_ConcatenatedNumber.append(p_Number);
		}

		for (String c_Iterator : v_ConcatenatedNumber.toString().split(""))
		{
			v_Counter += Integer.parseInt(c_Iterator);
		}

		return calculateResult(v_Counter);
	}

	private static int calculateResult(long p_Number)
	{
		if (p_Number < 10)
		{
			return (int) p_Number;
		}

		ArrayList<Integer> v_DigitList = new ArrayList<Integer>();
		splitDigits(p_Number, v_DigitList);
		int v_Result = sumDigits(v_DigitList);
		return v_Result < 10 ? v_Result : calculateResult((long) v_Result);
	}

	private static int sumDigits(ArrayList<Integer> p_DigitList)
	{
		int v_DigitSum = 0;
		for (Integer c_Iterator : p_DigitList)
		{
			v_DigitSum += c_Iterator.intValue();
		}
		return v_DigitSum;
	}

	private static void splitDigits(long l, ArrayList<Integer> p_DigitList)
	{
		if (l / 10 > 0)
		{
			splitDigits(l / 10, p_DigitList);
		}
		p_DigitList.add((int) (l % 10));
	}

	public static SingleDigit provideSingleDigit(String p_Number, int p_RepetitionFactor, User p_User)
	{
		SingleDigit v_SingleDigit = null;
		
		if(!validateNumberFormat(p_Number) || p_Number.length() >= Math.pow(10, 1000000) || p_RepetitionFactor >= 100000)
		{
			return v_SingleDigit;
		}

		if (p_User != null)
		{
			v_SingleDigit = p_User.getSingleDigit(p_Number, p_RepetitionFactor);
		}

		if (v_SingleDigit == null)
		{
			v_SingleDigit = getFromCache(p_Number, p_RepetitionFactor);

			if (v_SingleDigit == null)
			{
				v_SingleDigit = new SingleDigit(p_User == null ? null : p_User.getPK(), p_Number, p_RepetitionFactor,
						calculateSingleDigit(p_Number, p_RepetitionFactor));
				v_SingleDigit.putOnCache();
			}

			if (p_User != null)
			{
				v_SingleDigit.setUserPK(p_User.getPK());
				putOnDB(v_SingleDigit);
				p_User.addResult(v_SingleDigit);
			}
		}

		return v_SingleDigit;
	}

	private static boolean validateNumberFormat(String p_Number)
	{
		try
		{
			Long.parseLong(p_Number);
		}
		catch (NumberFormatException e)
		{
			System.out.println("Not a Number");
			return false;
		}
		return true;
	}

	private static void putOnDB(SingleDigit v_SingleDigit)
	{
		try
		{
			v_SingleDigit.setPK(UserTM.insertSingleDigit(v_SingleDigit));
		}
		catch (ClassNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	private void putOnCache()
	{
		m_Cache[m_CacheIterator] = this;
		m_CacheIterator = (++m_CacheIterator) % 10;
	}

	private static SingleDigit getFromCache(String p_Number, int p_RepetitionFactor)
	{
		for (SingleDigit c_SingleDigit : m_Cache)
		{
			if (c_SingleDigit != null && c_SingleDigit.getInputInteger().equalsIgnoreCase(p_Number)
					&& c_SingleDigit.getNumberOfRepetitions() == p_RepetitionFactor)
			{
				return c_SingleDigit;
			}
		}

		return null;
	}

	public static ArrayList<SingleDigit> listSDByUser(int p_UserPK) throws ClassNotFoundException, SQLException
	{
		return UserTM.listSDByUser(p_UserPK);
	}

	public static void deleteAll(ArrayList<SingleDigit> p_SingleDigitList) throws ClassNotFoundException, SQLException
	{
		for (SingleDigit c_SingleDigit : p_SingleDigitList)
		{
			c_SingleDigit.delete();
		}
	}

	private void delete() throws ClassNotFoundException, SQLException
	{
		UserTM.deleteSingleDigit(getSingleDigitPK());
	}
}
