package tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import br.com.wagsf.Model.SingleDigit;

public class SingleDigitTest
{

	@Before
	public void setUp() throws Exception
	{
		SingleDigit.provideSingleDigit("5", 2, null);
	}

	@Test
	public void testCalculateSingleDigit()
	{
		assertEquals(4, SingleDigit.calculateSingleDigit("4", 1));
	}

	@Test
	public void testCalculateSingleDigitCache()
	{
		assertEquals(1, SingleDigit.provideSingleDigit("5", 2, null).getResult());
	}
	
	@Test
	public void testCalculateSingleDigitOutOfBounds()
	{

		assertEquals(null, SingleDigit.provideSingleDigit("10000000", 200000, null) == null ? null
				: SingleDigit.provideSingleDigit("10000000", 200000, null).getResult());
	}

}
