package tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import br.com.wagsf.Model.SingleDigit;
import br.com.wagsf.Model.User;

public class UserTest
{
	User m_User;

	@Before
	public void setUp() throws Exception
	{
		m_User = User.provide("Teste", "teste@teste.com");
		m_User.addResult(new SingleDigit(m_User.getPK(), "1", 1, SingleDigit.calculateSingleDigit("1", 1)));
		m_User.addResult(new SingleDigit(m_User.getPK(), "2", 1, SingleDigit.calculateSingleDigit("2", 1)));
		m_User.addResult(new SingleDigit(m_User.getPK(), "1", 1, SingleDigit.calculateSingleDigit("1", 1)));
	}

	@Test
	public void testCalculateNewSingleDigit()
	{
		assertEquals(2, m_User.getSingleDigitList().size());
	}

	@After
	public void tearDown() throws Exception
	{
		m_User.deleteUser();
	}
}
